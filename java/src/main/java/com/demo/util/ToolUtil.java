package com.demo.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class ToolUtil {
	
	private static Random random = new Random();

	public static String getGen() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		return sdf.format(new Date());
	}
	
	public static String genTradeNo() {
		StringBuffer sb = new StringBuffer();
		sb.append(getGen());
		for(int i=0;i<6;i++	) {
			sb.append(random.nextInt(10));
		}
		return sb.toString();
	}
	
	public static String genNotr() {
		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		char[] strs = str.toCharArray();
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<10;i++) {
			sb.append(strs[random.nextInt(strs.length)]);
		}
		return sb.toString();
	}
}
