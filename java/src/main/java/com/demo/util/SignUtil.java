package com.demo.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 签名工具类
 *
 */
public class SignUtil {

	public static String signign(Map<String,String> reqParam, String key) {
		boolean result = false;
		Map<String, String> params = paraFilter(reqParam);
		StringBuilder buf = new StringBuilder((params.size() + 1) * 10);
		buildPayParams(buf, params, false);
		String preStr = buf.toString();
		String signRecieve = sign(preStr, "&key=" + key, "utf-8");
		return signRecieve;
	}
	
	public static String sign(String text, String key, String input_charset) {
		text = text + key;
		System.out.println("========待加签串为:"+text);
		return DigestUtils.md5Hex(getContentBytes(text, input_charset));
	}
	
	private static byte[] getContentBytes(String content, String charset) {
		if (charset == null || "".equals(charset)) {
			return content.getBytes();
		}
		try {
			return content.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
		}
	}
	
	public static Map<String, String> paraFilter(Map<String, String> sArray) {
		Map<String, String> result = new HashMap<String, String>(sArray.size());
		if (sArray == null || sArray.size() <= 0) {
			return result;
		}
		for (String key : sArray.keySet()) {
			String value = sArray.get(key);
			if (value == null || value.equals("")) {
				continue;
			}
			result.put(key, value);
		}
		return result;
	}
	
	public static void buildPayParams(StringBuilder sb, Map<String, String> payParams, boolean encoding) {
		List<String> keys = new ArrayList<String>(payParams.keySet());
		Collections.sort(keys);
		for (String key : keys) {
			String v = payParams.get(key);
			if (v == null || "".equals(v)) {
				continue;
			}
			sb.append(key).append("=");
			if (encoding) {
				sb.append(urlEncode(payParams.get(key)));
			} else {
				sb.append(payParams.get(key));
			}
			sb.append("&");
		}
		sb.setLength(sb.length() - 1);
	}
	
	public static String urlEncode(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8");
		} catch (Throwable e) {
			return str;
		}
	}
}
