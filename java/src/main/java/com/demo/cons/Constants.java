package com.demo.cons;
/**
 * 公共常量
 *
 */
public class Constants {
	//机构号
	public static final String SP_ID = "1008";
	//商户号
	public static final String MCHT_ID = "100870000000001";
	//测试秘钥
	public static final String SECRET_KEY = "A46999DBE22D46959CD4B945A0B0BAEF";
	//调用地址
	public static final String BASE_URL = "http://testapi.youfudata.cn/gate/";
	//绑卡地址
	public static final String BIND_URL = "epay/open";
	//绑卡确认地址
	public static final String BIND_CHECK_URL = "epay/openCheck";
	//快捷支付下单
	public static final String PAY_URL = "epay/apply";
	//快捷支付确认
	public static final String PAY_SUBMIT_URL = "epay/submit";
	//订单查询
	public static final String QRY_ORDER_URL = "spsvr/trade/qry";
}
