package com.demo.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.demo.cons.Constants;
import com.demo.util.HttpUtil;
import com.demo.util.SignUtil;
import com.demo.util.ToolUtil;

/**
 * 订单查询demo
 *
 */
public class QueryDemo {

	public static void main(String[] args) throws Exception{
		String url = Constants.BASE_URL + Constants.QRY_ORDER_URL;
		
		Map<String,String> reqParam = new HashMap<String,String>();
		reqParam.put("sp_id", Constants.SP_ID);
		reqParam.put("mch_id", Constants.MCHT_ID);
		reqParam.put("out_trade_no", "TL20171108102847561844");
		reqParam.put("nonce_str", ToolUtil.genNotr());
		reqParam.put("sign", SignUtil.signign(reqParam, Constants.SECRET_KEY));
		
		List<NameValuePair> params = new ArrayList<>();
		for(Map.Entry<String, String> tmp:reqParam.entrySet()) {
			params.add(new BasicNameValuePair(tmp.getKey(), tmp.getValue()));
		}
		
		byte[] res = HttpUtil.getInstance().doPost(url, null, params);
		String content = new String(res,"UTF-8");
		System.out.println("返回结果："+content);
		HttpUtil.getInstance().shutdown();
	}
}
