package com.demo.epay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.demo.cons.Constants;
import com.demo.util.HttpUtil;
import com.demo.util.SignUtil;
import com.demo.util.ToolUtil;

/**
 * 快捷绑卡demo
 *
 */
public class BindCardDemo {
	
	public static void main(String[] args) throws Exception{
		
		String url = Constants.BASE_URL + Constants.BIND_URL;
		
		Map<String,String> reqParam = new HashMap<String,String>();
		reqParam.put("sp_id", Constants.SP_ID);
		reqParam.put("mch_id", Constants.MCHT_ID);
		reqParam.put("out_trade_no", ToolUtil.genTradeNo());
		reqParam.put("acct_type", "DEBIT");
		reqParam.put("acc_name", "测试一");
		reqParam.put("acc_no", "");
		reqParam.put("id_no", "360782199112083513");
		reqParam.put("mobile", "18500609302");
		reqParam.put("expire_date", "");
		reqParam.put("cvv", "");
		reqParam.put("user_id", "1001");
		reqParam.put("nonce_str", ToolUtil.genNotr());
		reqParam.put("sign", SignUtil.signign(reqParam, Constants.SECRET_KEY));
		
		List<NameValuePair> params = new ArrayList<>();
		for(Map.Entry<String, String> tmp:reqParam.entrySet()) {
			params.add(new BasicNameValuePair(tmp.getKey(), tmp.getValue()));
		}
		
		byte[] res = HttpUtil.getInstance().doPost(url, null, params);
		String content = new String(res,"UTF-8");
		System.out.println("返回结果："+content);
		HttpUtil.getInstance().shutdown();
	}
}
