from urllib import request
from urllib import parse

import random

url = 'http://testapi.youfudata.cn/gate/gw/pay'

data = {
    "sp_id": "1002",
    "mch_id": "100270000000001",
    "out_trade_no": str(random.randint(100000000, 999999999)),
    "bank_code": "01050000",
    "goods_name": "iphone11",
    "notify_url": "http://localhost.com/test/",
    "call_back_url": "http://localhost.com/test",
    "total_fee": "14",
    "card_type": "DEBIT",  # CREDIT
    "user_type": 1,  # 1 个人 2 企业
    "channel": 1,  # 1 pc 2 mobile
    "nonce_str": "61611532897",
    "sign": "sign",
}

# bank code
# code:01050000  建设银行
# code:01030000  农业银行
# code:01020000  工商银行
# code:01040000  中国银行
# code:03100000  浦发银行
# code:03030000  光大银行
# code:03070000  平安银行
# code:03090000  兴业银行
# code:01000000  邮政储蓄银行
# code:03020000  中信银行
# code:03040000  华夏银行
# code:03080000  招商银行
# code:03060000  广发银行
# code:04031000  北京银行
# code:21  上海银行
# code:03050000  民生银行
# code:03010000  交通银行
# code:2  北京农村商业银行

print(str(data))
data = parse.urlencode(data).encode("utf-8")

req = request.Request(url=url, data=data)
resp = request.urlopen(req).read().decode("utf-8")

print(resp)

