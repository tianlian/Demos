package main

import (
	"strings"
	"io/ioutil"
	"net/http"
	"errors"
	"reflect"
	"bytes"
	"strconv"
	"math/rand"
	"fmt"
)

const (
	URL = "http://testapi.youfudata.cn/gate/gw/pay"
)

func main() {
	//"sp_id": "1002",
		//"mch_id": "100270000000001",
		//"out_trade_no": str(random.randint(100000000, 999999999)),
		//	"bank_code": "01050000",
		//	"goods_name": "iphone11",
		//	"notify_url": "http://localhost.com/test/",
		//	"call_back_url": "http://localhost.com/test",
		//	"total_fee": "14",
		//	"card_type": "DEBIT",  # CREDIT
		//"user_type": 1,  # 1 个人 2 企业
		//"channel": 1,  # 1 pc 2 mobile
		//"nonce_str": "61611532897",
		//"sign": "sign",
		//
	req := &GateGwPayRequest{
		Sp_id:"1002",
		Mch_id:"100270000000001",
		Out_trade_no:strconv.Itoa(rand.Int()),
		Bank_code:"01050000",
		Goods_name:"iphone11",
		Notify_url:"http://localhost.com/test/",
		Call_back_url:"http://localhost.com/test/",
		Total_fee:14,
		Card_type:"DEBIT",
		User_type:1,
		Channel:1,
		Nonce_str:"61611532897",
		Sign:"sign",
	}
	params ,err := EncodeRequestForData(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	result,err := SendRequestTestAPI(params,URL)
	if err!=nil{
		fmt.Println(err)
		return
	}
	fmt.Println(result)
}


type GateGwPayRequest struct {
	//字段名		类型	是否必填	字段名称	说明
	Sp_id         string //	String(32)	是	服务商号	系统分配的服务商号
	Mch_id        string //String(32)	是	商户号	由系统分配的商户号
	Out_trade_no  string //	String(32)	是	商户订单号	商户订单号，确保唯一
	Total_fee     int64  //Int	是	总金额	订单总金额，单位为分
	Goods_name          string //	Stirng	是	商品名称	商品名称
	Notify_url    string //	String	是	通知地址	快捷支付提交成功后回调的通知地址：务必确保外网可以访问
	Call_back_url string //
	User_type     int64 //	String	是	1 个人 2 企业
	Card_type	string //DEBIT CREDIT
	Channel       int64 // 1 PC 2 MOBILE
	Bank_code     string //	String 	是	银行编码	银行编码银行列表
	Nonce_str     string //(32)	是	随机字符串	随机字符串，不长于 32 位
	Sign          string //(32)	是	签名	MD5 签名结果，详见“签名说明”
}

func SendRequestTestAPI(params, url string) (string, error) {
	resp, err := http.Post(url,
		"application/x-www-form-urlencoded",
		strings.NewReader(params))
	if err != nil {
		return "",err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.New("request post failed")
	}

	return string(body), nil
}

func EncodeRequestForData(i interface{}) (string, error) {
	s := reflect.ValueOf(i)
	//t := s.Type()
	f := s.Elem()
	//tableName := strings.Split(t.String(), ".")[1]

	bf := bytes.Buffer{}
	for i := 0; i < f.NumField(); i++ {
		if i > 0 {
			bf.WriteString("&")
		}
		bf.WriteString(strings.ToLower(f.Type().Field(i).Name))
		bf.WriteString("=")
		switch f.Field(i).Kind() {
		case reflect.String:
			bf.WriteString(f.Field(i).String())
		case reflect.Int64:
			bf.WriteString(strconv.Itoa(int(f.Field(i).Int())))
		}

	}


	var err error
	if err != nil {
		return "", err
	}
	return bf.String(), nil
}